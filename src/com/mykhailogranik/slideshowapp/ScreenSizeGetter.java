package com.mykhailogranik.slideshowapp;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Class is used for getting the relevant height and width of the screen
 */
@EBean(scope = Scope.Singleton)
public class ScreenSizeGetter {

	private int width;

	private int height;

	@RootContext
	Context context;

	public int getWidth() {
		setScreenParameters();
		return width;
	}

	public int getHeight() {
		setScreenParameters();
		return height;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@AfterInject
	public void setScreenParameters() {
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();
		if (android.os.Build.VERSION.SDK_INT >= 13) {
			Point size = new Point();
			display.getSize(size);
			width = size.x;
			height = size.y;
		} else {
			width = display.getWidth();
			height = display.getHeight();
		}
	}

}
