package com.mykhailogranik.slideshowapp;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Interface for working with SharedPrefferences in Android Annotations way
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface SlideShowSettings {

	int period();

	@DefaultBoolean(true)
	boolean fromStoage();

	@DefaultBoolean(false)
	boolean loadOnBooting();

	@DefaultBoolean(false)
	boolean loadOnCharging();

	@DefaultInt(0)
	int startSlideShowTime();

	@DefaultInt(600)
	int slideShowDuration();

	@DefaultString("")
	String addresses();

}
