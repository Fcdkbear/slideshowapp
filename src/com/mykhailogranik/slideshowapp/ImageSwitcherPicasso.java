package com.mykhailogranik.slideshowapp;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageSwitcher;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * A class, that helps to load images directly to ImageSwitcher object using
 * Picasso Lib1rary
 */
@EBean
public class ImageSwitcherPicasso implements Target {

	private ImageSwitcher imageSwitcher;

	@RootContext
	SlideShowActivity context;

	public void setmImageSwitcher(ImageSwitcher imageSwitcher) {
		this.imageSwitcher = imageSwitcher;
	}

	@Override
	public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
		imageSwitcher.setImageDrawable(new BitmapDrawable(context.getResources(), bitmap));
	}

	@Override
	public void onBitmapFailed(Drawable drawable) {

	}

	@Override
	public void onPrepareLoad(Drawable drawable) {

	}

}