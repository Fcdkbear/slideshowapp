package com.mykhailogranik.slideshowapp;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

/**
 * Custom class, that implements target. Used while downloading images.
 * <p>
 * It calculates the number of downloaded images and calls the necessary
 * callback method in the SettingsActivity
 */
public class CustomTarget implements Target {

	public static int totalDownloaded = 0;

	public static int needToDownload;

	private int id;

	private SettingsActivity context;

	public CustomTarget(SettingsActivity context, int id) {
		this.context = context;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public void onBitmapFailed(Drawable arg0) {
		// TODO Auto-generated method stub
		processDownloadedImage();
	}

	@Override
	public void onBitmapLoaded(Bitmap arg0, LoadedFrom arg1) {
		processDownloadedImage();
	}

	void processDownloadedImage() {
		totalDownloaded++;
		Log.i("Downloaded", totalDownloaded + "");
		if (totalDownloaded == needToDownload) {
			context.onDownloadingComplete();
		}
	}

	@Override
	public void onPrepareLoad(Drawable arg0) {

	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof CustomTarget) {
			return (((CustomTarget) o).getId() == id);
		}
		return false;
	}

}
