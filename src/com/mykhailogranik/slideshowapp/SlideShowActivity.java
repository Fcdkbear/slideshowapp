package com.mykhailogranik.slideshowapp;

import java.util.Timer;
import java.util.TimerTask;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import android.app.Activity;
import android.content.res.Configuration;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher.ViewFactory;

import com.squareup.picasso.Picasso;

/**
 * Activity shows the slides to the user
 */
@EActivity(R.layout.activity_slide_show_layout)
@WindowFeature(Window.FEATURE_NO_TITLE)
public class SlideShowActivity extends Activity {

	@ViewById(R.id.topImageSwitcher)
	ImageSwitcher topImageSwitcher;

	@ViewById(R.id.downImageSwitcher)
	ImageSwitcher downImageSwitcher;

	@Bean
	ImageSwitcherPicasso topImageSwitcherPicasso;

	@Bean
	ImageSwitcherPicasso downImageSwitcherPicasso;

	@Bean
	ImagePathGetter imagePathGetter;

	@Bean
	ScreenSizeGetter screeScrenSizeGetter;

	@Extra("period")
	int period;

	Timer timer;

	@AfterViews
	public void startWork() {
		tuneImageSwitcher(topImageSwitcher);
		topImageSwitcherPicasso.setmImageSwitcher(topImageSwitcher);
		if (isTablet() && isInPortraitOrientation()) {
			tuneImageSwitcher(downImageSwitcher);
			downImageSwitcherPicasso.setmImageSwitcher(downImageSwitcher);
		} else {
			downImageSwitcher.setVisibility(View.GONE);
		}
		runTimer();
	}

	public void tuneImageSwitcher(ImageSwitcher switcher) {
		switcher.setFactory(new ViewFactory() {

			@Override
			public View makeView() {
				ImageView myView = new ImageView(getApplicationContext());
				return myView;
			}
		});
		Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
		Animation out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
		switcher.setInAnimation(in);
		switcher.setOutAnimation(out);
	}

	/**
	 * Methods starts running the timer, that switches the slides
	 */
	private void runTimer() {
		timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				switchImage();
			}
		}, 0, period * 1000);
	}

	private boolean isInPortraitOrientation() {
		return (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
	}

	private boolean isTablet() {
		return (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	/**
	 * Switches the images on the screen
	 */
	@UiThread
	public void switchImage() {
		int actualHeight = screeScrenSizeGetter.getHeight();
		if (isTablet() && isInPortraitOrientation()) {
			actualHeight /= 2;
		}
		if (isTablet() && isInPortraitOrientation()) {
			String previousPath = imagePathGetter.getPreviousPath();
			if (previousPath != null) {
				Uri previousUri = Uri.parse(previousPath);
				Picasso.with(this).load(previousUri).resize(screeScrenSizeGetter.getWidth(), actualHeight)
						.into(downImageSwitcherPicasso);
			} else {
				downImageSwitcher.setImageDrawable(null);
			}
		}
		String path = imagePathGetter.getPath();
		Uri uri = Uri.parse(path);
		Picasso.with(this).load(uri).resize(screeScrenSizeGetter.getWidth(), actualHeight)
				.into(topImageSwitcherPicasso);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		timer.cancel();
	}
}
