package com.mykhailogranik.slideshowapp;

import java.util.Date;

import org.androidannotations.annotations.EBean;

import com.mykhailograqnik.slideshowapp.receivers.SlideShowStartReceiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Class is used for setting the start time of the slide show
 */
@EBean
public class SlideShowStartTimeSetter {

	/**
	 * Sets the start time of the slide show. AlarmManager is used for starting
	 * the slide show at the appropriate time
	 * 
	 * @param context
	 *            The context, that calls this method
	 * @param seconds
	 *            Indicates the time, when the slide show must be started (in
	 *            seconds, from the moment, when the images were downloaded).
	 * @param period
	 *            Time period between two consecutive slides
	 * 
	 */
	public void setTime(Context context, int seconds, int period) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Date futureDate = new Date(new Date().getTime() + seconds * 1000);
		Intent intent = new Intent(context, SlideShowStartReceiver.class);
		intent.putExtra("period", period);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.set(AlarmManager.RTC_WAKEUP, futureDate.getTime(), sender);
	}

}
