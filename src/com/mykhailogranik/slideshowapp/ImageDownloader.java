package com.mykhailogranik.slideshowapp;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.net.Uri;

import com.squareup.picasso.Picasso;

/**
 * Class is used for downloading the images
 */
@EBean
public class ImageDownloader {

	private Uri[] uris;

	@RootContext
	SettingsActivity context;

	@Bean
	ScreenSizeGetter screenSizeGetter;

	private CustomTarget[] targets;

	int position = 0;

	/**
	 * Converts string addresses to uris
	 * 
	 * @param addressesAsString
	 *            string, that represents addresses of the images (from web or
	 *            from local folder)
	 * @param fromStorage
	 *            true if the source of images is local folder, false otherwise
	 */
	public void setAddresses(String addressesAsString, boolean fromStorage) {
		String[] addresses = addressesAsString.split("\\s+");
		uris = new Uri[addresses.length];
		for (int i = 0; i < addresses.length; ++i) {
			if (fromStorage) {
				uris[i] = Uri.parse("file://" + addresses[i]);
			} else {
				uris[i] = Uri.parse(addresses[i]);
			}
		}
	}

	/**
	 * Downloads all images from addresses, specified at setAddresses method
	 */
	public void downloadAllImages() {
		CustomTarget.needToDownload = uris.length;
		CustomTarget.totalDownloaded = 0;
		targets = new CustomTarget[uris.length];
		for (int i = 0; i < targets.length; ++i) {
			targets[i] = new CustomTarget(context, i);
		}
		for (int i = 0; i < uris.length; ++i) {
			Picasso.with(context).load(uris[i]).resize(screenSizeGetter.getWidth(), screenSizeGetter.getHeight())
					.centerCrop().into(targets[i]);
		}
	}
}
