package com.mykhailogranik.slideshowapp;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Class is used for getting the paths of the images for slide show
 */
@EBean(scope = Scope.Singleton)
public class ImagePathGetter {

	private String[] addresses;

	private boolean fromStorage;

	public void setAddresses(String addressesAsString, boolean fromStorage) {
		position = 0;
		this.fromStorage = fromStorage;
		addresses = addressesAsString.split("\\s+");
	}

	@RootContext
	Context context;

	ImageView imageView;

	Drawable drawable;

	private static int position = 0;

	/**
	 * @return the path to the current image to be displayed on a slide
	 */
	public String getPath() {
		String result = addresses[position];
		if (fromStorage) {
			result = "file://" + result;
		}
		position = (position + 1) % addresses.length;
		return result;
	}

	/**
	 * Use this method to get the path to the image to be displayed on the
	 * second slide on a screen (for tablets in portrait mode)
	 * 
	 * @return the path to the previous image to be displayed on secondary slide
	 */
	public String getPreviousPath() {
		int previousPosition = position - 1;
		if (previousPosition < 0) {
			return null;
		}
		return addresses[previousPosition];
	}

}
