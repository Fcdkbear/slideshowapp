package com.mykhailogranik.slideshowapp;

import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.mykhailograqnik.slideshowapp.receivers.SlideShowStopReceiver;

/**
 * Class is used for setting the duration of the slide show
 */
public class SlideShowDurationSetter {

	/**
	 * Sets the duration of the slide show. AlarmManager is used for stopping
	 * the slide show at the appropriate time
	 * 
	 * @param context
	 *            The context, that calls this method
	 * @param seconds
	 *            Indicates the time, when the slide show must be stopped (in
	 *            seconds, from the moment, when the images were downloaded).
	 */
	public void setTime(Context context, int seconds) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Date futureDate = new Date(new Date().getTime() + seconds * 1000);
		Intent intent = new Intent(context, SlideShowStopReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.set(AlarmManager.RTC_WAKEUP, futureDate.getTime(), sender);
	}

}
