package com.mykhailogranik.slideshowapp;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Activity is used for setting the parameters of the slideshow
 */
@EActivity(R.layout.activity_settings_layout)
public class SettingsActivity extends Activity {

	@ViewById(R.id.periodEditText)
	EditText periodEditText;

	@ViewById(R.id.progressBar)
	ProgressBar progressBar;

	@ViewById(R.id.addressesEditText)
	EditText addressesEditText;

	@ViewById(R.id.fromStorage)
	RadioButton fromStorageRadioButton;

	@ViewById(R.id.fromWeb)
	RadioButton fromWebRadioButton;

	@ViewById(R.id.imageSourceRadioGroup)
	RadioGroup imageSourceRadioGroup;

	@ViewById(R.id.runOnBootingCheckBox)
	CheckBox runOnBootingCheckBox;

	@ViewById(R.id.runOnChargingCheckBox)
	CheckBox runOnChargingCheckBox;

	@ViewById(R.id.startSlideShowEditText)
	EditText startSlideShowEditText;

	@ViewById(R.id.slideShowDurationEditText)
	EditText slideShowDurationEditText;

	@StringRes(R.string.period_error)
	String periodErrorString;

	@Bean
	ImagePathGetter imagePathGetter;

	@Bean
	ImageDownloader imageDownloader;

	@Pref
	SlideShowSettings_ settings;

	public static final int MINIMUM_ALLOWED_PERIOD = 1;
	public static final int MAXIMUM_ALLOWED_PERIOD = 60;

	/**
	 * Loads the settings from the SharedPreferences
	 */
	@AfterViews
	public void init() {
		int savedPeriod = settings.period().get();
		if (savedPeriod != 0) {
			periodEditText.setText("" + savedPeriod);
		}
		boolean savedFromStorage = settings.fromStoage().get();
		if (savedFromStorage) {
			imageSourceRadioGroup.check(R.id.fromStorage);
		} else {
			imageSourceRadioGroup.check(R.id.fromWeb);
		}
		startSlideShowEditText.setText("" + settings.startSlideShowTime().get());
		slideShowDurationEditText.setText("" + settings.slideShowDuration().get());
		addressesEditText.setText(settings.addresses().get());
		runOnBootingCheckBox.setChecked(settings.loadOnBooting().get());
		runOnChargingCheckBox.setChecked(settings.loadOnCharging().get());
	}

	/**
	 * Starts a slideshow with user-specified parameters
	 */
	@Click(R.id.startSlideshowButton)
	public void startSlideShow() {
		int period = Integer.parseInt(periodEditText.getText().toString());
		if ((period < MINIMUM_ALLOWED_PERIOD) || (period > MAXIMUM_ALLOWED_PERIOD)) {
			setErrorForEditText(periodEditText, periodErrorString);
			return;
		}
		String addressesAsString = addressesEditText.getEditableText().toString();
		boolean fromStorage = fromStorageRadioButton.isChecked();
		int startSlideShowTime = Integer.parseInt(startSlideShowEditText.getText().toString());
		int slideShowDuration = Integer.parseInt(slideShowDurationEditText.getText().toString());
		imageDownloader.setAddresses(addressesAsString, fromStorage);
		imagePathGetter.setAddresses(addressesAsString, fromStorage);
		progressBar.setVisibility(View.VISIBLE);
		saveSettings(period, fromStorage, startSlideShowTime, slideShowDuration);
		imageDownloader.downloadAllImages();
	}

	/**
	 * Saves settings in SharedPreferences
	 * 
	 * @param period
	 *            time period between two consecutive slides
	 * @param fromStorage
	 *            true if the source of images is local folder, false otherwise
	 * @param startSlideShowTime
	 *            specifies the time in seconds between finishing the
	 *            downloading process and starting of the slide show
	 * @param slideShowDuration
	 *            specifies the duration of the slide show
	 */
	public void saveSettings(int period, boolean fromStorage, int startSlideShowTime, int slideShowDuration) {
		settings.edit().period().put(period).fromStoage().put(fromStorage).loadOnBooting()
				.put(runOnBootingCheckBox.isChecked()).loadOnCharging().put(runOnChargingCheckBox.isChecked())
				.startSlideShowTime().put(startSlideShowTime).slideShowDuration().put(slideShowDuration).addresses()
				.put(addressesEditText.getText().toString()).apply();
	}

	/**
	 * Method is called, when all images are downloaded
	 */
	public void onDownloadingComplete() {
		progressBar.setVisibility(View.INVISIBLE);
		new SlideShowStartTimeSetter().setTime(this, Integer.parseInt(startSlideShowEditText.getText().toString()),
				Integer.parseInt(periodEditText.getText().toString()));
		new SlideShowDurationSetter().setTime(this, Integer.parseInt(startSlideShowEditText.getText().toString())
				+ Integer.parseInt(slideShowDurationEditText.getText().toString()));
	}

	private void setErrorForEditText(EditText editText, String errorMessage) {
		ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);
		SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(errorMessage);
		spannableStringBuilder.setSpan(foregroundColorSpan, 0, errorMessage.length(), 0);
		editText.setError(spannableStringBuilder);
	}

}
