package com.mykhailograqnik.slideshowapp.receivers;

import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.sharedpreferences.Pref;

import com.mykhailogranik.slideshowapp.SettingsActivity_;
import com.mykhailogranik.slideshowapp.SlideShowSettings_;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Receiver is used for starting the slide show at the appropriate time
 */
@EReceiver
public class PowerConnectionReceiver extends BroadcastReceiver {

	@Pref
	SlideShowSettings_ settings;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (settings.loadOnCharging().get()) {
			Intent launchIntent = new Intent(context, SettingsActivity_.class);
			launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(launchIntent);
		}
	}

}
