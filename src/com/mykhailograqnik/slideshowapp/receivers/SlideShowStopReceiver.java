package com.mykhailograqnik.slideshowapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mykhailogranik.slideshowapp.SettingsActivity_;

/**
 * Receiver is used for stopping the slide show at the appropriate time
 */
public class SlideShowStopReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent launchIntent = new Intent(context, SettingsActivity_.class);
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(launchIntent);
	}

}
