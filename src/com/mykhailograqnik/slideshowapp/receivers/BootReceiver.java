package com.mykhailograqnik.slideshowapp.receivers;

import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mykhailogranik.slideshowapp.SettingsActivity_;
import com.mykhailogranik.slideshowapp.SlideShowSettings_;

/**
 * Receiver is used for loading the application after booting in completed
 */
@EReceiver
public class BootReceiver extends BroadcastReceiver {

	@Pref
	SlideShowSettings_ settings;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (settings.loadOnBooting().get()) {
			Intent launchIntent = new Intent(context, SettingsActivity_.class);
			launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(launchIntent);
		}
	}

}
