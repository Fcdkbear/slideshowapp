package com.mykhailograqnik.slideshowapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mykhailogranik.slideshowapp.SlideShowActivity_;

public class SlideShowStartReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent launchIntent = new Intent(context, SlideShowActivity_.class);
		int period = intent.getIntExtra("period", 0);
		launchIntent.putExtra("period", period);
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(launchIntent);
	}

}
